
INSERT INTO m_brand ( name, description, created) VALUES ('ZARA', 'Cadena de tiendas de Zara', NOW());

INSERT INTO m_currency ( name, description, created) VALUES ('EUR', 'Euros', NOW());
INSERT INTO m_currency ( name, description, created) VALUES ('USD', 'Dolares Americanos', NOW());

INSERT INTO m_product (id, name, description, created) VALUES (35455,'Camisa', 'Camisa colección de Verano', NOW());

INSERT INTO prices( brand_id, currency_id, product_id, start_date, end_date, priority, prices, created) VALUES ( 1, 1, 35455, '2020-06-14 00:00:00', '2020-12-31 23:59:59', 0, 35.50, NOW() );
INSERT INTO prices( brand_id, currency_id, product_id, start_date, end_date, priority, prices, created) VALUES ( 1, 1, 35455, '2020-06-14 15:00:00', '2020-06-14 18:30:00', 1, 25.45, NOW() );
INSERT INTO prices( brand_id, currency_id, product_id, start_date, end_date, priority, prices, created) VALUES ( 1, 1, 35455, '2020-06-15 00:00:00', '2020-06-15 11:00:00', 1, 30.50, NOW() );
INSERT INTO prices( brand_id, currency_id, product_id, start_date, end_date, priority, prices, created) VALUES ( 1, 1, 35455, '2020-06-15 16:00:00', '2020-12-31 23:59:59', 1, 38.95, NOW() );
