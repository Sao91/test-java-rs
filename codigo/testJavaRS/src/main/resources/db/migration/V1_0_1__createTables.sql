CREATE TABLE IF NOT EXISTS m_brand ( 
	id                   int  NOT NULL  AUTO_INCREMENT  PRIMARY KEY,
	name                 varchar(100)  NOT NULL    ,
	description          varchar(500)      ,
	created              timestamp  NOT NULL    ,
	modified             timestamp      ,
	deleted              timestamp      
 ) ;



CREATE TABLE IF NOT EXISTS m_currency ( 
	id                   int  NOT NULL  AUTO_INCREMENT  PRIMARY KEY,
	name                 varchar(100)  NOT NULL    ,
	description          varchar(500)      ,
	created              timestamp  NOT NULL    ,
	modified             timestamp      ,
	deleted              timestamp      
 ) ;


CREATE TABLE IF NOT EXISTS m_product ( 
	id                   int  NOT NULL  AUTO_INCREMENT  PRIMARY KEY,
	name                 varchar(100)  NOT NULL    ,
	description          varchar(500)      ,
	created              timestamp  NOT NULL    ,
	modified             timestamp      ,
	deleted              timestamp      
 ) ;



CREATE TABLE IF NOT EXISTS prices ( 
	id                   bigint  NOT NULL  AUTO_INCREMENT  PRIMARY KEY,
	brand_id             int  NOT NULL    ,
	currency_id          int  NOT NULL    ,
	product_id           int  NOT NULL    ,
	start_date           timestamp  NOT NULL    ,
	end_date             timestamp  NOT NULL    ,
	priority             int   DEFAULT 0   ,
	prices               decimal(12,2)  NOT NULL    ,
	created              timestamp  NOT NULL    ,
	modified             timestamp      ,
	deleted              timestamp      
 ) ;



ALTER TABLE prices ADD CONSTRAINT IF NOT EXISTS fk_prices_m_brand FOREIGN KEY ( brand_id ) REFERENCES m_brand( id ) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE prices ADD CONSTRAINT IF NOT EXISTS fk_prices_m_currency FOREIGN KEY ( currency_id ) REFERENCES m_currency( id ) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE prices ADD CONSTRAINT IF NOT EXISTS fk_prices_m_product FOREIGN KEY ( product_id ) REFERENCES m_product( id ) ON DELETE NO ACTION ON UPDATE NO ACTION;
