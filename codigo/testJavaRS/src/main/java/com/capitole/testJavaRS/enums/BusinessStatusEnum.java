/**
 * 
 */
package com.capitole.testJavaRS.enums;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

/**
 * @author Ricardo Santos
 *
 */
@Getter @ToString @AllArgsConstructor
public enum BusinessStatusEnum {

	OK("200","Ok"),
	BAD_REQUEST("400","Bad Request"),
	UNAUTHORIZED("401","Unauthorized"),
	FORBIDDEN("403","Forbidden"),
	NOT_FOUND("404","Not Found"),
	REQUEST_TIMEOUT("408","Request Timeout"),
	TOO_MANY_REQUESTS("429","Too many requests"),
	ERROR("500","Internal Server Error");

	private String cod;
	private String name;
	
	private static final Map<String, BusinessStatusEnum> lookup = new LinkedHashMap<>();

    static {
        for (BusinessStatusEnum s : EnumSet.allOf(BusinessStatusEnum.class))
            lookup.put(s.getCod(), s);
    }
    
	public static BusinessStatusEnum findByCodigo(String codigo) {
        return lookup.get(codigo);
    }


    public static List<BusinessStatusEnum> findAll() {
        return new ArrayList<>(lookup.values());
    }
}
