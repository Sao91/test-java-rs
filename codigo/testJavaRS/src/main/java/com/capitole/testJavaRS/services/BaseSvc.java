/**
 * 
 */
package com.capitole.testJavaRS.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Ricardo Santos
 *
 */
@Slf4j
public abstract class BaseSvc <T, ID, R extends JpaRepository<T, ID>> {
	
	@Autowired
	protected R repository;
	
	public T save(T t) {
		log.info("Guardando nuevos registros ->{}",t);
		return repository.save(t);
	}
	
	public Optional<T> findBy(ID id){
		log.info("Busqueda por id ->{}",id);
		return repository.findById(id);
	}
	
	public List<T> findAll(){
		log.info("Listar todos los registros");
		return repository.findAll();
	}
	
	public List<T> findAll(Sort sort){
		log.info("Listar todos los registros ordenados ->",sort);
		return repository.findAll(sort);
	}
	
	public Page<T> findAll(Pageable pageable){
		log.info("Listar los registros paginados ->",pageable);
		return repository.findAll(pageable);
	}
	
	public List<T> findAll(Example<T> example){
		return repository.findAll(example);
	}
	
	public Page<T> findAll(Example<T> example,Pageable pageable){
		return repository.findAll(example, pageable);
	}
	
	public List<T> findAll(Example<T> example,Sort sort){
		return repository.findAll(example, sort);
	}
	
	public T edit(T t) {
		log.info("Actualizando registros existentes ->{}",t);
		return repository.save(t);
	}
	
	public Boolean exist(ID id) {
		return repository.existsById(id);
	}
	
	public Boolean exist(Example<T> example) {
		return repository.exists(example);
	}
	
	public Long count() {
		return repository.count();
	}
	
	public Long count(Example<T> example ) {
		return repository.count(example);
	}
	
	public void delete(T t) {
		repository.delete(t);
	}
	
	public void deleteAll() {
		repository.deleteAll();
	}
	
	public boolean deleteById(ID id) {
		boolean flag;
		log.info("Eliminar ->",id);
		try {
			repository.deleteById(id);
			flag=true;
		} catch (Exception e) {
			log.info("Error message ->{}  StackTrace->{} ",e.getMessage(),e.getStackTrace());
			flag= false;
		}
		
		return flag;
	}
}
