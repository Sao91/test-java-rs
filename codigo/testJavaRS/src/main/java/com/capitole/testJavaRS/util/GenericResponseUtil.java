/**
 * 
 */
package com.capitole.testJavaRS.util;

import java.io.Serializable;
import java.time.LocalDateTime;

import lombok.Data;

/**
 * @author Ricardo Santos
 *
 */
@Data
public class GenericResponseUtil <T> implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private StatusResponseUtil statusResponse;
	private T result;
	private String message;
	private LocalDateTime timestamp;

}
