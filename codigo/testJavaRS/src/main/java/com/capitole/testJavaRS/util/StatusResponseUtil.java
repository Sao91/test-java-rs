/**
 * 
 */
package com.capitole.testJavaRS.util;

import com.capitole.testJavaRS.enums.BusinessStatusEnum;

import lombok.Data;

/**
 * @author Ricardo Santos
 *
 */
@Data
public class StatusResponseUtil {
	
	private String code;
    private String message;
    
    public StatusResponseUtil(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public StatusResponseUtil(BusinessStatusEnum businessStatus) {
    	this.code = businessStatus.getCod();
        this.message = businessStatus.getName();
    }
    
    public StatusResponseUtil(BusinessStatusEnum businessStatus, String message) {
    	this.code = businessStatus.getCod();
        this.message = message;
    }

}
