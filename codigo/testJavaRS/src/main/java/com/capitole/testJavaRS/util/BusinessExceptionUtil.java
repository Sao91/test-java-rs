/**
 * 
 */
package com.capitole.testJavaRS.util;

/**
 * @author Ricardo Santos
 *
 */
public class BusinessExceptionUtil extends RuntimeException {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	public BusinessExceptionUtil(String message) {
        super(message);
    }


    public BusinessExceptionUtil(String format, Object... message) {
        super(String.format(format, message));
    }
}
