/**
 * 
 */
package com.capitole.testJavaRS.services;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.capitole.testJavaRS.dto.FilterPricesDto;
import com.capitole.testJavaRS.dto.PriceSearchDto;
import com.capitole.testJavaRS.model.PricesModel;
import com.capitole.testJavaRS.repository.PricesRepository;
import com.capitole.testJavaRS.services.interfaces.PricesISvc;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Ricardo Santos
 *
 */
@Slf4j
@Service 
public class PricesSvc extends BaseSvc<PricesModel, Long, PricesRepository> implements PricesISvc{
	
	
	@Override
	public PriceSearchDto findPrice(FilterPricesDto filterDto) {
		
		
		// TODO Auto-generated method stub
		
		Optional<List<PricesModel>> findPrice =this.repository.findPrice(filterDto);
		
		if(!findPrice.isPresent())
			throw new ResponseStatusException(HttpStatus.NO_CONTENT, "No existe resultados en la consulta");
		
		if(findPrice.get().size()==0)
			throw new ResponseStatusException(HttpStatus.NO_CONTENT, "No existe resultados en la consulta");
		
		log.info("Prueba -> {}",findPrice.get().toString());
		
		if(findPrice.get().size()>1) {
			PricesModel pricesModel=findPrice.get().stream().max(Comparator.comparing(PricesModel::getPriority)).get();
			return  responsePriceSearchDto(pricesModel);
		}
		
		
		return  responsePriceSearchDto(findPrice.get().get(0));
				
	}
	
	private PriceSearchDto responsePriceSearchDto(PricesModel pricesModel) {
		
		return PriceSearchDto.builder()
				.priceId(pricesModel.getId())
				.brandId(pricesModel.getBrand().getId())
				.endDate(pricesModel.getEndDate())
				.startDate(pricesModel.getStartDate())
				.productId(pricesModel.getProduct().getId())
				.price(pricesModel.getPrices())
				.build();
	}

}
