/**
 * 
 */
package com.capitole.testJavaRS.services.interfaces.master;

import com.capitole.testJavaRS.model.master.BrandModel;
import com.capitole.testJavaRS.services.interfaces.BaseISvc;

/**
 * @author Ricardo Santos
 *
 */
public interface BrandISvc extends BaseISvc<BrandModel, Integer>{

}
