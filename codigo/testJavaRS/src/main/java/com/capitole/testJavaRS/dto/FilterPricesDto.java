/**
 * 
 */
package com.capitole.testJavaRS.dto;

import java.io.Serializable;
import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author Ricardo Santos
 *
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class FilterPricesDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int brandId;
	
	private int productId;
	
	private LocalDateTime applicableDate;
	

}
