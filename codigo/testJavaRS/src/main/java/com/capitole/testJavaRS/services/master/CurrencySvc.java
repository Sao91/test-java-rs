/**
 * 
 */
package com.capitole.testJavaRS.services.master;

import org.springframework.stereotype.Service;

import com.capitole.testJavaRS.model.master.CurrencyModel;
import com.capitole.testJavaRS.repository.master.CurrencyRepository;
import com.capitole.testJavaRS.services.BaseSvc;
import com.capitole.testJavaRS.services.interfaces.master.CurrencyISvc;

/**
 * @author Ricardo Santos
 *
 */
@Service 
public class CurrencySvc extends BaseSvc<CurrencyModel, Integer, CurrencyRepository> implements CurrencyISvc{

}
