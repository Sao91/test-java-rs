/**
 * 
 */
package com.capitole.testJavaRS.services.interfaces;

import com.capitole.testJavaRS.dto.FilterPricesDto;
import com.capitole.testJavaRS.dto.PriceSearchDto;
import com.capitole.testJavaRS.model.PricesModel;

/**
 * @author Ricardo Santos
 *
 */
public interface PricesISvc extends BaseISvc<PricesModel, Long>{

	
	public PriceSearchDto findPrice(FilterPricesDto filterDto);
}
