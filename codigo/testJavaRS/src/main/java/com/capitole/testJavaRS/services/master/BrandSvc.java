/**
 * 
 */
package com.capitole.testJavaRS.services.master;

import org.springframework.stereotype.Service;

import com.capitole.testJavaRS.model.master.BrandModel;
import com.capitole.testJavaRS.repository.master.BrandRepository;
import com.capitole.testJavaRS.services.BaseSvc;
import com.capitole.testJavaRS.services.interfaces.master.BrandISvc;

/**
 * @author Ricardo Santos
 *
 */
@Service 
public class BrandSvc extends BaseSvc<BrandModel, Integer, BrandRepository> implements BrandISvc{

}
