/**
 * 
 */
package com.capitole.testJavaRS.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

/**
 * @author Ricardo Santos
 *
 */
@MappedSuperclass
@Data 
@NoArgsConstructor 
@AllArgsConstructor 
@SuperBuilder 
@ToString
public class PrincipalSuperClassModel implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@CreationTimestamp
	@Column(name="created",  nullable = false, updatable = false, columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private LocalDateTime created;
	
	
	@UpdateTimestamp
	@Column(name="modified", insertable = false, updatable = true,columnDefinition="TIMESTAMP NULL")
	private LocalDateTime modified;
	
	
	@Column(name="deleted", columnDefinition="TIMESTAMP NULL")
	private LocalDateTime deleted;
	

}
