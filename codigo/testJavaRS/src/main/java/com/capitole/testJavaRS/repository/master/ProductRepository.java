/**
 * 
 */
package com.capitole.testJavaRS.repository.master;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.capitole.testJavaRS.model.master.ProductModel;

/**
 * @author Ricardo Santos
 *
 */
@Repository
public interface ProductRepository extends JpaRepository<ProductModel, Integer>{

}
