/**
 * 
 */
package com.capitole.testJavaRS.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;

import com.capitole.testJavaRS.model.master.BrandModel;
import com.capitole.testJavaRS.model.master.CurrencyModel;
import com.capitole.testJavaRS.model.master.ProductModel;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

/**
 * @author Ricardo Santos
 *
 */
@Entity
@Table(name = "prices")
@Getter 
@Setter 
@NoArgsConstructor 
@AllArgsConstructor 
@ToString 
@SuperBuilder
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
public class PricesModel extends PrincipalSuperClassModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int priority;
	
	private BigDecimal prices;
	
	@CreationTimestamp
	@Column(name="start_date",  nullable = false, updatable = false, columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private LocalDateTime startDate;
	
	@CreationTimestamp
	@Column(name="end_date",  nullable = false, updatable = false, columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private LocalDateTime endDate;
	
	@ToString.Exclude
	@JsonManagedReference
	@ManyToOne(fetch=FetchType.LAZY, cascade=CascadeType.ALL)
	@JoinColumn(name="brand_id")
	private BrandModel brand;
	
	@ToString.Exclude
	@JsonManagedReference
	@ManyToOne(fetch=FetchType.LAZY, cascade=CascadeType.ALL)
	@JoinColumn(name="currency_id")
	private CurrencyModel currency;
	
	@ToString.Exclude
	@JsonManagedReference
	@ManyToOne(fetch=FetchType.LAZY, cascade=CascadeType.ALL)
	@JoinColumn(name="product_id")
	private ProductModel product;

}
