/**
 * 
 */
package com.capitole.testJavaRS.repository.master;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.capitole.testJavaRS.model.master.CurrencyModel;

/**
 * @author Ricardo Santos
 *
 */
@Repository
public interface CurrencyRepository extends JpaRepository<CurrencyModel, Integer>{

}
