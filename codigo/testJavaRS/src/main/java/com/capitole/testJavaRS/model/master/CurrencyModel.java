/**
 * 
 */
package com.capitole.testJavaRS.model.master;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

/**
 * @author Ricardo Santos
 *
 */
@Entity
@Table(name = "m_currency")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString 
@SuperBuilder
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
public class CurrencyModel extends MasterPrincipalSuperClassModel{
	 
	private static final long serialVersionUID = 1L;

	private String name;

	private String description;
}
