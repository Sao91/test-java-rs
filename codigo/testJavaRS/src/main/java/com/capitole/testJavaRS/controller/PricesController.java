/**
 * 
 */
package com.capitole.testJavaRS.controller;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.SortDefault;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.capitole.testJavaRS.constant.SystemConstant;
import com.capitole.testJavaRS.dto.FilterPricesDto;
import com.capitole.testJavaRS.dto.PriceSearchDto;
import com.capitole.testJavaRS.enums.BusinessStatusEnum;
import com.capitole.testJavaRS.model.PricesModel;
import com.capitole.testJavaRS.services.interfaces.PricesISvc;
import com.capitole.testJavaRS.util.GenericResponseUtil;
import com.capitole.testJavaRS.util.StatusResponseUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Ricardo Santos
 *
 */
@Slf4j
@RestController
@RequestMapping("/prices/")
public class PricesController {
	
	@Autowired
	private PricesISvc pricesService;
	
	@GetMapping({ "all" })
	public ResponseEntity<?> all(@RequestParam(value="page", defaultValue="0") int page,
			@SortDefault.SortDefaults({
				@SortDefault(sort = "id", direction = Sort.Direction.DESC)
			}) Sort sort){
		
		
		Pageable pageable = PageRequest.of(page, SystemConstant.DEFAULT_PAGE_SIZE, sort);
		log.info("Pageable ->{}",pageable);
		GenericResponseUtil<Page<PricesModel>> response = new GenericResponseUtil<>();
		response.setStatusResponse(new StatusResponseUtil(BusinessStatusEnum.OK));
		response.setResult(pricesService.findAll(pageable));
		return ResponseEntity.ok(response);
		
	}
	
	@GetMapping({ "search" })
	public ResponseEntity<?> search(
			@RequestParam(required=true) int productId,
			@RequestParam(required=true) int brandId,
			@RequestParam(required=true) @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss") LocalDateTime applicableDate
	        ) {
		
		
		FilterPricesDto filterDto=FilterPricesDto.builder()
				.brandId(brandId)
				.productId(productId)
				.applicableDate(applicableDate)
				.build();
		
		log.info("filterDto ->{}",filterDto);
		
		GenericResponseUtil<PriceSearchDto> response = new GenericResponseUtil<>();
		response.setStatusResponse(new StatusResponseUtil(BusinessStatusEnum.OK));
		response.setResult(pricesService.findPrice(filterDto));
		return ResponseEntity.ok(response);
	}
	
	
	
}
