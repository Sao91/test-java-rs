/**
 * 
 */
package com.capitole.testJavaRS.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author Ricardo Santos
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class PriceSearchDto implements Serializable{/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private long priceId;
	
	private int productId;
	
	private int brandId;
	
	private LocalDateTime startDate;
	
	private LocalDateTime endDate;
	
	private BigDecimal price;

}
