/**
 * 
 */
package com.capitole.testJavaRS.services.interfaces;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

/**
 * @author Ricardo Santos
 *
 */
public interface BaseISvc<T, ID> {
	
	public T save(T t);
	
	public Optional<T> findBy(ID id);
	
	public List<T> findAll();
	
	public List<T> findAll(Sort sort);
	
	public Page<T> findAll(Pageable pageable);
	
	public List<T> findAll(Example<T> example);
	
	public Page<T> findAll(Example<T> example,Pageable pageable);
	
	public List<T> findAll(Example<T> example,Sort sort);
	
	public T edit(T t);
	
	public Boolean exist(ID id) ;
	
	public Boolean exist(Example<T> example) ;
	
	public Long count();
	
	public Long count(Example<T> example );
	
	public void delete(T t);
	
	public void deleteAll();
	
	public boolean deleteById(ID id);

}
