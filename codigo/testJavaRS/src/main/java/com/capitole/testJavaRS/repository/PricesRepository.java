/**
 * 
 */
package com.capitole.testJavaRS.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.capitole.testJavaRS.dto.FilterPricesDto;
import com.capitole.testJavaRS.model.PricesModel;

/**
 * @author Ricardo Santos
 *
 */
@Repository
public interface PricesRepository extends JpaRepository<PricesModel, Long> {
	
	
	@Query("SELECT p FROM PricesModel p "+
			"JOIN p.brand b "+
			"JOIN p.product pr "+
			"WHERE b.id= :#{#filterDto.brandId} AND pr.id= :#{#filterDto.productId} "+
			"AND p.startDate < :#{#filterDto.applicableDate} AND p.endDate > :#{#filterDto.applicableDate} "+
			"ORDER BY p.priority DESC"
			)
	Optional<List<PricesModel>> findPrice(FilterPricesDto filterDto);

}
