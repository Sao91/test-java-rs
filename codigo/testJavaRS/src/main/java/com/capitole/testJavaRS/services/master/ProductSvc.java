/**
 * 
 */
package com.capitole.testJavaRS.services.master;

import org.springframework.stereotype.Service;

import com.capitole.testJavaRS.model.master.ProductModel;
import com.capitole.testJavaRS.repository.master.ProductRepository;
import com.capitole.testJavaRS.services.BaseSvc;
import com.capitole.testJavaRS.services.interfaces.master.ProductISvc;

/**
 * @author Ricardo Santos
 *
 */
@Service 
public class ProductSvc extends BaseSvc<ProductModel, Integer, ProductRepository> implements ProductISvc{

}
