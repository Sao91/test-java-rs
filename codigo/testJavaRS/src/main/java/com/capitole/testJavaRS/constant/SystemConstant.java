/**
 * 
 */
package com.capitole.testJavaRS.constant;

/**
 * @author Ricardo Santos
 *
 */
public class SystemConstant {

	public static final int DEFAULT_PAGE_SIZE = 10;
}
