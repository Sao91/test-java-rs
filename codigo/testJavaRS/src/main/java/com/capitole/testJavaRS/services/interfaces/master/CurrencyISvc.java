/**
 * 
 */
package com.capitole.testJavaRS.services.interfaces.master;

import com.capitole.testJavaRS.model.master.CurrencyModel;
import com.capitole.testJavaRS.services.interfaces.BaseISvc;

/**
 * @author Ricardo Santos
 *
 */
public interface CurrencyISvc extends BaseISvc<CurrencyModel, Integer>{

}
