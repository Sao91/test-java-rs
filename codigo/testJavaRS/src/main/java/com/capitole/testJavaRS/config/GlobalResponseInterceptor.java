/**
 * 
 */
package com.capitole.testJavaRS.config;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.capitole.testJavaRS.enums.BusinessStatusEnum;
import com.capitole.testJavaRS.util.GenericResponseUtil;
import com.capitole.testJavaRS.util.StatusResponseUtil;


/**
 * @author Ricardo Santos
 *
 */

@ControllerAdvice
public class GlobalResponseInterceptor extends ResponseEntityExceptionHandler{
	
	private final  Logger log = LoggerFactory.getLogger(this.getClass());	
	
	GenericResponseUtil<?> response = new GenericResponseUtil<>();
	
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(
	        MethodArgumentNotValidException ex,
	        HttpHeaders headers,
	        HttpStatus status,
	        WebRequest request) {
		log.error("[ERROR] MethodArgumentNotValidException  -> Message of System : {}  .... print Stack Tracer  -> {}",ex.getMessage(),ex.getStackTrace());
	    BindingResult result = ex.getBindingResult();
	    List<String> message =result.getFieldErrors().stream().map(FieldError::getDefaultMessage).collect(Collectors.toList());
	    response.setTimestamp(LocalDateTime.now());
		response.setStatusResponse(new StatusResponseUtil(BusinessStatusEnum.BAD_REQUEST));
		response.setMessage(message.toString().replace("[", "").replace("]", ""));
		HttpHeaders headder=new HttpHeaders();
		headder.add("Content-Type", "application/json; charset=utf-8");
	    return handleExceptionInternal(ex,response,headder, HttpStatus.BAD_REQUEST, request);
	}
	
	@Override
	protected ResponseEntity<Object> handleServletRequestBindingException(
			  ServletRequestBindingException ex, 
			  HttpHeaders headers, 
			  HttpStatus status, 
			  WebRequest request) {

		log.error("[ERROR] ServletRequestBindingException  -> Message of System : {}  .... print Stack Tracer  -> {}",ex.getMessage(),ex.getStackTrace());
	    response.setTimestamp(LocalDateTime.now());
		response.setStatusResponse(new StatusResponseUtil(BusinessStatusEnum.BAD_REQUEST));
		HttpHeaders headder=new HttpHeaders();
		headder.add("Content-Type", "application/json; charset=utf-8");
	    return handleExceptionInternal(ex,response,headder, HttpStatus.BAD_REQUEST, request);
	}
	  
	@ExceptionHandler(ResponseStatusException.class)
	public ResponseEntity<GenericResponseUtil<?>> responseStatusException(ResponseStatusException e) {
		e.printStackTrace();
		log.error("[ERROR] ResponseStatusException  -> Message of System : {}  .... Message of exception  -> {}  .... print Stack Tracer  -> {}",e.getReason(),e.getMessage(),e.getStackTrace());
		response.setTimestamp(LocalDateTime.now());
		response.setStatusResponse(new StatusResponseUtil(BusinessStatusEnum.findByCodigo(String.valueOf(e.getStatus().value()))));
		response.setMessage(e.getReason());
		HttpHeaders headder=new HttpHeaders();
		headder.add("Content-Type", "application/json; charset=utf-8");
		return new ResponseEntity<>(response,headder, e.getStatus());
	}

	
	@ExceptionHandler(MaxUploadSizeExceededException.class)
	public ResponseEntity<GenericResponseUtil<?>> maxUploadSizeExceededException(MaxUploadSizeExceededException e) {
		e.printStackTrace();
		log.error("[ERROR] MaxUploadSizeExceededException  -> Message of System : {}  .... print Stack Tracer  -> {}",e.getMessage(),e.getStackTrace());
		response.setTimestamp(LocalDateTime.now());
		response.setStatusResponse(new StatusResponseUtil(BusinessStatusEnum.ERROR));
		response.setMessage("El archivo a superado el limite máximo de subida de 10 MB");
		HttpHeaders headder=new HttpHeaders();
		headder.add("Content-Type", "application/json; charset=utf-8");
		return new ResponseEntity<>(response, headder, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler(RuntimeException.class)
	public ResponseEntity<GenericResponseUtil<?>> runtimeException(RuntimeException e) {
		e.printStackTrace();
		log.error("[ERROR] RuntimeException  -> Message of System : {}  .... print Stack Tracer  -> {}",e.getMessage(),e.getStackTrace());
		response.setTimestamp(LocalDateTime.now());
		response.setStatusResponse(new StatusResponseUtil(BusinessStatusEnum.ERROR));
		response.setMessage(e.getMessage());
		HttpHeaders headder=new HttpHeaders();
		headder.add("Content-Type", "application/json; charset=utf-8");
		return new ResponseEntity<>(response, headder, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler(Exception.class)
	public ResponseEntity<GenericResponseUtil<?>> exception(Exception e) {
		e.printStackTrace();
		log.error("[ERROR] Exception  -> Message of System : {}  .... print Stack Tracer  -> {}",e.getMessage(),e.getStackTrace());
		response.setTimestamp(LocalDateTime.now());
		response.setStatusResponse(new StatusResponseUtil(BusinessStatusEnum.ERROR));
		response.setMessage(e.getMessage());
		HttpHeaders headder=new HttpHeaders();
		headder.add("Content-Type", "application/json; charset=utf-8");
		return new ResponseEntity<>(response,headder, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
}
