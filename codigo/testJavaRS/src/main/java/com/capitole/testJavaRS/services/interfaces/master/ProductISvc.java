/**
 * 
 */
package com.capitole.testJavaRS.services.interfaces.master;

import com.capitole.testJavaRS.model.master.ProductModel;
import com.capitole.testJavaRS.services.interfaces.BaseISvc;

/**
 * @author Ricardo Santos
 *
 */
public interface ProductISvc extends BaseISvc<ProductModel, Integer>{

}
