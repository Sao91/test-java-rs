/**
 * 
 */
package com.capitole.testJavaRS;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Ricardo Santos
 *
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class PricesControllerTests {
	
	@Autowired
    private MockMvc mockMvc;
	 
	@Sql({ "classpath:db/migration/V1_0_1__createTables.sql","classpath:db/migration/V2_0_1__insertTables.sql" })
	@Test
    public void test1() throws Exception {
		
		log.info("Test 1: petición a las 10:00 del día 14 del producto 35455   para la brand 1 (ZARA)");
	                
        String response = mockMvc.perform(get("/prices/search?productId=35455&brandId=1&applicableDate=2020-06-14 10:00:00"))
                .andExpect(status().is(HttpStatus.OK.value()))
                .andExpect(jsonPath("$.result.priceId").value(1))
                .andExpect(jsonPath("$.result.price").value(35.5))
                .andReturn().getResponse().getContentAsString();
 
        log.info("response: " + response);
    }
	
	@Test
    public void test2() throws Exception {
		
		log.info("Test 2: petición a las 16:00 del día 14 del producto 35455   para la brand 1 (ZARA)");
	                
        String response = mockMvc.perform(get("/prices/search?productId=35455&brandId=1&applicableDate=2020-06-14 16:00:00"))
                .andExpect(status().is(HttpStatus.OK.value()))
                .andExpect(jsonPath("$.result.priceId").value(2))
                .andExpect(jsonPath("$.result.price").value(25.45))
                .andReturn().getResponse().getContentAsString();
 
        log.info("response: " + response);
    }
	
	@Test
    public void test3() throws Exception {
		
		log.info("Test 3: petición a las 21:00 del día 14 del producto 35455   para la brand 1 (ZARA)");
	                
        String response = mockMvc.perform(get("/prices/search?productId=35455&brandId=1&applicableDate=2020-06-14 21:00:00"))
                .andExpect(status().is(HttpStatus.OK.value()))
                .andExpect(jsonPath("$.result.priceId").value(1))
                .andExpect(jsonPath("$.result.price").value(35.5))
                .andReturn().getResponse().getContentAsString();
 
        log.info("response: " + response);
    }
	
	@Test
    public void test4() throws Exception {
		
		log.info("Test 4: petición a las 10:00 del día 15 del producto 35455   para la brand 1 (ZARA)");
	                
        String response = mockMvc.perform(get("/prices/search?productId=35455&brandId=1&applicableDate=2020-06-15 10:00:00"))
                .andExpect(status().is(HttpStatus.OK.value()))
                .andExpect(jsonPath("$.result.priceId").value(3))
                .andExpect(jsonPath("$.result.price").value(30.5))
                .andReturn().getResponse().getContentAsString();
 
        log.info("response: " + response);
    }
	
	@Test
    public void test5() throws Exception {
		
		log.info("Test 5: petición a las 21:00 del día 16 del producto 35455   para la brand 1 (ZARA)");
	                
        String response = mockMvc.perform(get("/prices/search?productId=35455&brandId=1&applicableDate=2020-06-16 21:00:00"))
                .andExpect(status().is(HttpStatus.OK.value()))
                .andExpect(jsonPath("$.result.priceId").value(4))
                .andExpect(jsonPath("$.result.price").value(38.95))
                .andReturn().getResponse().getContentAsString();
 
        log.info("response: " + response);
    }
}


